from .base import *

from decouple import config

# Database
# https://docs.djangoproject.com/en/2.2/ref/settings/#databases

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.postgresql_psycopg2',
        'NAME': config('LOCAL_DB_NAME'),
        'USER': config('LOCAL_DB_USER'),
        'PASSWORD': config('LOCAL_DB_PASSWORD'),
        'HOST': '127.0.0.1',
        'PORT': 5432,
    },
}
